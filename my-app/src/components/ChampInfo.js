import React from "react";
import { Link } from "react-router-dom";
import defaultImg from "../images/room-1.jpeg";
import PropTypes from "prop-types";

export default function ChampInfo({ champ }) {
    const { name, slug, images, price } = champ;
    return (
        <article className="room">
            <div className="img-container">
                <img src={images[0] || defaultImg} alt="champion" />
                <Link to={`/Champions/${slug}`} className="btn-primary room-link"> Features </Link>
            </div>
            <p className="room-info">{name}</p>
        </article>
    );
}
