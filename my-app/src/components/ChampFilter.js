import React from "react";
import { useContext } from "react";
import { ChampContext } from "../context";
import Title from "../components/Title";

//get all unique values
const getUnique = (items, value) => {
  return [...new Set(items.map(item => item[value]))];
};

export default function ChampFilter({champs}){
  const context = useContext(ChampContext);
  const {
    handleChange,
    type,
  } = context;
// get unique types
  let types = getUnique(champs, 'type');
  // add all
  types = ['all',...types];
  //map to jsx
  types = types.map((item,index)=> {
    return <option value={item} key={index}>{item}</option>
  })
  return (
        <section className="filter-container">
            <Title title="Champions" />
            <form className="filter-form" id="grid">
              {/*select type*/}
              <div className="form-group-right">
                <div className="form-group" id="">
                <label htmlFor="type"> champ type</label>
                  <select name="type" id="type" value={type} className="form-control" onChange={handleChange}>
                    {types}
                  </select>
                </div>
              </div>
              {/* end select type*/}
            </form>
        </section>
  )
}
