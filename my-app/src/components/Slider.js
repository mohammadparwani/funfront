import React, { Component } from 'react'
import logo from '../images/logo.svg'
import {FaAlignRight} from 'react-icons/fa'
import {Link} from 'react-router-dom'

export default class Slider extends
    Component {
    render() {
        return (
            <div className="slider-frame">
                <div className="slide-images">
                    <div className="img-container">
                        <img src="Warwick.jpg"/>
                    </div>

                    <div className="img-container">
                        <img src="Zed.jpg"/>
                    </div>

                    <div className="img-container">
                        <img src="Thresh.jpg"/>
                    </div>

                </div>
            </div>
        )
    }
}
