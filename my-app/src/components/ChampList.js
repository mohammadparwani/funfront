import React from 'react';
import Champ from './Champ'
//import ChampFilter from './ChampFilter';

export default function ChampList({champs}){
if(champs.length ===0){
  return (
    <div className="empty-search">
      <h3>Geen champ gevonden</h3>
    </div>
  );
}

return (
  <section className="roomslist">
    <div className="roomslist-center">
        {champs.map(item => {
          return <Champ key={item.id} champ={item} />
        })}
    </div>
  </section>
)
}
