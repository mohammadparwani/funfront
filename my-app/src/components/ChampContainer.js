import React from 'react'
import ChampFilter from './ChampFilter';
import DipFilter from './DipFilter';
import ChampList from './ChampList';
import {withChampConsumer} from '../context';
import Loading from './Loading';

function ChampContainer({context}){
  const {loading, sortedChamps, champs} = context;
  if(loading) {
    return <Loading />;
  }
  return (
    <>
     {<ChampFilter  champs={champs}/>}
     <ChampList  champs={sortedChamps} />
    </ >
  );
}

export default withChampConsumer(ChampContainer)
