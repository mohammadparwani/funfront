import React, { Component } from 'react';
import { ChampContext } from "../context";
import Loading from "./Loading";
import Champ from "./Champ";
import Title from "./Title";

export default class FeaturedChamps extends Component {
	static contextType = ChampContext;
	render() {
		let { loading, featuredChamps: champs } = this.context;
		champs = champs.map(champ => {
			return <Champ key={champ.id} champ={champ} />;
		});
		return (
			<section className="featured-rooms">

				<Title title="featured Champs" />
				<div className="featured-rooms-center">
					{loading ? <Loading /> : champs}
				</div>
			</section>
		);
	}
}