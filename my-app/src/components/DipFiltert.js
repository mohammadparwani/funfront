import React, { Component } from 'react'
import { useContext } from "react";
import { ChampContext } from "../context";
import Title from "../components/Title";
import PostData from './posts.json'

class DipFilter extends Component {

constructor (props){
super(props)
this.toggleSortDate = this.toggleSortDate.bind(this)
this.state = {
    postList: [],
    isOldestFirst: true
  }
}

sortByDate () {
  const {postList} = this.state
  let newPostList = postList
  if (this.state.isOldestFirst) {
    newPostList = postList.sort((a, b) => a.name > b.name)
  } else {
    newPostList = postList.sort((a, b) => a.name < b.name)
  }
  this.setState({
    isOldestFirst: !this.state.isOldestFirst,
    postList: newPostList
  })
}

toggleSortDate (event) {
  this.sortByDate()
}

componentDidMount () {
  const postList = PostData
  this.setState({
    isOldestFirst: true,
    postList: postList
  })
}
  render () {
        const {postList} = this.state
  return (
        <section className="filter-container">
        <div>
        <button onClick={this.toggleSortDate}>Order by date</button>
          {postList.map((item) => {
            console.log({item})
          })}
        </div>
        </section>
  )
}}

export default DipFilter
