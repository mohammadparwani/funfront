import React from "react";
import { Link } from "react-router-dom";
import defaultImg from "../images/room-1.jpeg";
import PropTypes from "prop-types";

export default function Champ({ champ }) {
    const { name, slug, images, price } = champ;
    return (
        <article className="room">
            <div className="img-container">
                <img src={images[0] || defaultImg} alt="champion" height={300} width={270}/>
                <Link to={`/Champions/${slug}`}  id="btn-primary room-link" className="btn-primary room-link"> Features </Link>
            </div>
            <p className="room-info">{name}</p>
        </article>
    );
}

Champ.prototype = {
    room: PropTypes.shape({
        name: PropTypes.string.isRequired,
        slug: PropTypes.string.isRequired,
        images: PropTypes.arrayOf(PropTypes.string).isRequired,
        price: PropTypes.number.isRequired,
    })
}
