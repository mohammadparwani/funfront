import React, { Component } from 'react'
import logo from '../images/logo.svg'
import {FaAlignRight} from 'react-icons/fa'
import {Link} from 'react-router-dom'
import ListOverView from "../pages/ListOverView";

export default class Navbar extends
    Component {
    render() {
        return (
            <div className="nav-wrapper">
                <div className="left-side">
                    <div className="nav-link-wrapper active-nav-link">
                        <Link to="/">Home</Link>
                    </div>

                    <div className="nav-link-wrapper">
                        <Link to="Champions">Champions</Link>
                    </div>

                    <div className="nav-link-wrapper">
                        <Link to="AddChamp">AddChamp</Link>
                    </div>

                    <div className="nav-link-wrapper">
                        <Link to="ListOverView">Champ Overview</Link>
                    </div>

                    <div className="nav-link-wrapper">
                        <Link to="MyProfile">Profile</Link>
                    </div>

                </div>

                <div className="right-side">
                  <div className="brand">
                      <div className="login-container">
                        <Link to="Login">Login</Link>
                      </div>
                  </div>
                        {/* Search form */}
                        <div className="md-form mt-0" id="test">
                            <input className="form-control" type="text" placeholder="Search" aria-label="Search" />
                        </div>
                </div>
            </div>
            )
    }
}
