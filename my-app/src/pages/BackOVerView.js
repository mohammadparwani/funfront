import React, { Component } from 'react';
import {Bootstrap, Grid, Row, Col} from 'react-bootstrap';
import { Button, FormGroup, FormControl, FormLabel } from "react-bootstrap";
import'bootstrap/dist/css/bootstrap.css';
import Champ from "../components/Champ";

const mensen = [{"name":"Moo"}, {"name":"Mauk"}, {"name":"Sybe"}];

class ListOverView extends Component{
    constructor(props){
        super(props);
        this.state = {
            students: [
                { id: 1, name: 'Wasif', age: 21, email: 'wasif@email.com' },
                { id: 2, name: 'Ali', age: 19, email: 'ali@email.com' },
                { id: 3, name: 'Saad', age: 16, email: 'saad@email.com' },
                { id: 4, name: 'Asad', age: 25, email: 'asad@email.com' }
            ]
        }
    }

    changeChampion = () =>{
        this.setState({

        })
    };

    deleteChampion = () =>{
        this.setState({

        })
    };

    renderTableData = () => {
        return this.state.students.map((student, index) => {
            const { id, name, age, email } = student //destructuring
            return (
                <tr key={id}>
                    <td>{id}</td>
                    <td>{name}</td>
                    <td>{age}</td>
                    <td>{email}</td>
                </tr>
            )
        })
    }


    render(){
        return (
            <>
                <div className="ListOverView-wrapper">
                    <table className="table table-striped">

                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">First</th>
                            <th scope="col">Last</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td><button className="btn-primary" id="deleteChamp" onClick={this.deleteChampion.bind(this)}>X</button> <button className="btn-primary" id="changeChamp" onClick={this.changeChampion.bind(this)}>X</button></td>
                        </tr>
                        <tr>
                            <th scope="row">2</th>
                            <td>Jacob</td>
                            <td>Thornton</td>
                            <td>@fat</td>
                        </tr>
                        <tr>
                            <th scope="row">3</th>
                            <td>Larry</td>
                            <td>the Bird</td>
                            <td>@twitter</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </>
        );
    }
}

export default ListOverView;
