import React, { Component } from 'react';
import {Bootstrap, Grid, Row, Col} from 'react-bootstrap';
import { Button, FormGroup, FormControl, FormLabel } from "react-bootstrap";
import'bootstrap/dist/css/bootstrap.css';
import {Link} from "react-router-dom";
import axios from "axios";


class ListOverView extends Component{
    constructor(props){
        super(props);
        this.state = {
            students: [],
            act: 0,
            index: '',
        }
    }

    componentDidMount() {
        this.getAllUsers();
    }

    //get data from database and put in the inputfields


    getAllUsers= async () =>{
        axios.get('//')
            .then(res =>{
                console.log(res);
                for (const dataObj of res.data){
                    //this.state.members.push(dataObj.login);
                    this.setState({students:this.state.students.concat(
                            {
                                description:dataObj.description,
                                name:dataObj.name,
                                id:dataObj.id,
                                creation:dataObj.creation,
                                updateDate:dataObj.changeDate,
                            })})
                }
                console.log(this.state.students);
            })
            .catch(function (error) {
                console.log(error);
            });
    };


    changeChampion = (i) =>{
        alert('Mauk is glitch')
    };

    deleteChampion = (i) =>{
        let students = this.state.students;
        students.splice(i,1);
        this.setState({
            students: students
        });
    };

    renderTableData = () => {
        return this.state.students.map((student, index) => {
            const { id, name, lastName, email, action } = student //destructuring
            return (
                <tr key={id}>
                    <td>{id}</td>
                    <td>{name}</td>
                    <td>{lastName}</td>
                    <td>{email}</td>
                    <td><button className="btn-primary" id="deleteChamp" onClick={()=>this.deleteChampion(index)}>{action}</button> <Link to="MyProfile"><button className="btn-primary" id="changeChamp" onClick={()=>this.changeChampion(index)}>{action}</button></Link></td>
                </tr>
            )
        })
    }

    renderTableHeader = () => {
        let header = Object.keys(this.state.students[0])
        return header.map((key, index) => {
            return <th key={index}>{key.toUpperCase()}</th>
        })
    }


    render(){
        return (
            <>
                <div className="ListOverView-wrapper">
                    <div>
                        <h1 id='title'>React Dynamic Table</h1>
                        <table id='students' className="table table-striped">
                            <tbody>
                            <tr>{this.renderTableHeader()}</tr>
                            {this.renderTableData()}
                            </tbody>
                        </table>
                    </div>
                </div>
            </>
        );
    }
}

export default ListOverView;
