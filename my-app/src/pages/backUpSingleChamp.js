import React, { Component } from "react";
import defaultBcg from '../images/room-1.jpeg';
import Hero from "../components/Hero";
import Banner from "../components/Banner";
import { Link } from "react-router-dom";
import { ChampContext } from "../context"
import StyledHero from "../components/StyledHero";
import { FaCocktail, FaHiking, FaShuttleVan, FaBeer } from "react-icons/fa";

export default class SingleChamp extends Component {
    constructor(props) {
        super(props)
        //console.log(this.props)
        this.state = {
            slug: this.props.match.params.slug,
            defaultBcg
        };
    }


    static contextType = ChampContext;

    componentDidMount() {
    }

    render() {
        const { getChamp } = this.context;
        const champ = getChamp(this.state.slug);

        if(!champ){
            return  <div className="error">
                <h3>Champ not found</h3>
            </div>
        }

        const {name, type, description, images} = champ;
        const [mainImg] = images;
        console.log(mainImg);

        return	(
            <>

                <div class="content-wrapper">
                    <div class="built-items-wrapper">



                        <div class="built-item-wrapper">
                            <div class="built-img-background">
                                {images.map((item, index) =>{
                                    return <img key={index} src={item} alt={name}/>
                                })}
                            </div>

                            <div class="img-text-wrapper">

                            </div>
                        </div>


                        <div class="built-item-wrapper">
                            <div class="InsideBuildsItem">
                                <div class="InsideBuild">
                                    <div className="single-champ-wrapper">
                                        <div className="testIets">
                                            <ul className="random">
                                                <li className="hwEUco">
                                                    <div className="animation-img">
                                                        <FaCocktail className="arnie"/>
                                                    </div>
                                                    <div className="champ-rol">
                                                        Role: {type}
                                                    </div>
                                                </li>
                                                <li></li>
                                            </ul>
                                        </div>
                                        <div className="fKThfc"></div>
                                        <div className="hZPZqS">
                                            <p>
                                                While many other yordles channel their energy into discovery, invention, or just plain mischief-making, Tristana was always inspired by the adventures of great warriors. She had heard much about Runeterra, its factions, and its wars, and believed her kind could become worthy of legend too.
                                            </p>
                                        </div>
                                    </div>
                                </div>



                                <div className="InsideBuild">
                                    <div className="single-champ-wrapper">
                                        <div className="testIets">
                                            <h2 className="headerName">Abilities</h2>
                                            <div className="abilities-container-wrapper">
                                                <div className="abilities-container">
                                                    <div className="abilities">
                                                        <button>
                                                            <span></span>
                                                            <span></span>
                                                            <span></span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div className="fKThfc"></div>
                                        <div className="hZPZqS">
                                            <p>
                                                <div className="test-img-background">
                                                    {images.map((item, index) => {
                                                        return <img key={index} src={item} alt={name}/>
                                                    })}
                                                </div>
                                            </p>
                                        </div>
                                    </div>
                                </div>



                                <div className="InsideBuild">
                                    <div className="single-champ-wrapper">
                                        <div className="single-champ-skins-wrapper">
                                            <div className="skrr">
                                                <h2 className="headerName">Abilities</h2>
                                                <div className="test-img-background">
                                                    {images.map((item, index) => {
                                                        return <img key={index} src={item} alt={name}/>
                                                    })}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </>

        )
    }
}

//238
