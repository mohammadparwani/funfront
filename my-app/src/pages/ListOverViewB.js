import React, { Component } from 'react';
import {Bootstrap, Grid, Row, Col} from 'react-bootstrap';
import { Button, FormGroup, FormControl, FormLabel } from "react-bootstrap";
import'bootstrap/dist/css/bootstrap.css';
import {Link} from "react-router-dom";


class ListOverView extends Component{
    constructor(props){
        super(props);
        this.state = {
            students: [
                { id: 1, name: 'Wasif', lastName: 'parwani', email: 'wasif@email.com', action: 'X'},
                { id: 2, name: 'Ali',   lastName: 'parwani', email: 'ali@email.com', action: 'X'},
                { id: 3, name: 'Saad',  lastName: 'parwani', email: 'saad@email.com', action: 'X'},
                { id: 4, name: 'Asad',  lastName: 'parwani', email: 'asad@email.com', action: 'X'}
            ],
            act: 0,
            index: '',
        }
    }

    //get data from database and put in the inputfields

    changeChampion = (i) =>{
        alert('Mauk is glitch')
    };

    deleteChampion = (i) =>{
        let students = this.state.students;
        students.splice(i,1);
        this.setState({
            students: students
        });
    };

    renderTableData = () => {
        return this.state.students.map((student, index) => {
            const { id, name, lastName, email, action } = student //destructuring
            return (
                <tr key={id}>
                    <td>{id}</td>
                    <td>{name}</td>
                    <td>{lastName}</td>
                    <td>{email}</td>
                    <td><button className="btn-primary" id="deleteChamp" onClick={()=>this.deleteChampion(index)}>{action}</button> <Link to="MyProfile"><button className="btn-primary" id="changeChamp" onClick={()=>this.changeChampion(index)}>{action}</button></Link></td>
                </tr>
            )
        })
    }

    renderTableHeader = () => {
        let header = Object.keys(this.state.students[0])
        return header.map((key, index) => {
            return <th key={index}>{key.toUpperCase()}</th>
        })
    }


    render(){
        return (
            <>
                <div className="ListOverView-wrapper">
                    <div>
                        <h1 id='title'>React Dynamic Table</h1>
                        <table id='students' className="table table-striped">
                            <tbody>
                            <tr>{this.renderTableHeader()}</tr>
                            {this.renderTableData()}
                            </tbody>
                        </table>
                    </div>
                </div>
            </>
        );
    }
}

export default ListOverView;
