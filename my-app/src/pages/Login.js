import React, { Component } from 'react';
import { Button, FormGroup, FormControl, FormLabel } from "react-bootstrap";
import "../Login.css";
import LoginBox from '../components/LoginBox';
import RegisterBox from '../components/RegisterBox';
/*
import LoginBox from './LoginBox';
import RegisterBox from './RegisterBox';
*/



class LoginPage extends Component{
  constructor(props){
    super(props);
      this.state = { isLoginOpen: true, isRegisterOpen: false};
  }

  showLoginBox = () =>{
    this.setState({isLoginOpen: true, isRegisterOpen: false})
  };

  showRegisterBox = () =>{
    this.setState({isRegisterOpen: true, isLoginOpen: false})
  };

  submitLogin = (e) =>{
  };

  render(){
    return (
      <>
      <div className="Login">
       <div className="form-box">
               <div className="choice-wrapper">
                 <div className="choice" onClick={this.showLoginBox.bind(this)}>
                     <p>Login</p>
                 </div>
                 <div className="choice" onClick={this.showRegisterBox.bind(this)}>
                     <p>Register</p>
                 </div>
              </div>
              {this.state.isLoginOpen && <LoginBox />}
              {this.state.isRegisterOpen && <RegisterBox />}
          </div>
        </div>
      </>
  );
  }
}

export default LoginPage;
