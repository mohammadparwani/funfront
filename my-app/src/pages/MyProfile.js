import React, { Component } from 'react';
import { Button, FormGroup, FormControl, FormLabel } from "react-bootstrap";
import "../Login.css";
import Title from "../components/Title";


class MyProfile extends Component{
    constructor(props){
        super(props);
        this.state = {
            students: [
                { id: 1, name: 'Wasif', lastName: 'parwani', email: 'wasif@email.com', action: 'X'},
                { id: 2, name: 'Ali',   lastName: 'parwani', email: 'ali@email.com', action: 'X'},
                { id: 3, name: 'Saad',  lastName: 'parwani', email: 'saad@email.com', action: 'X'},
                { id: 4, name: 'Asad',  lastName: 'parwani', email: 'asad@email.com', action: 'X'}
            ],
            act: 0,
            index: '',
        }
    }

    changeProfile = (i) =>{
        let student = this.state.students[i];
        this.refs.name.value = student.name;
        this.refs.lastname.value = student.lastName;
        this.refs.email.value = student.email;
        this.refs.action.value = student.action;

        this.setState({
            act: 1,
            index: i
        });
    };

    submitLogin = (e) =>{
    };

    render(){
        return (
            <>
                <Title title="Profile" />
                <div className="Login">
                    <div className="form-box">
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Name</label>
                            <input type="name" ref="name" className="form-control" id="exampleInputEmail1"
                                   aria-describedby="emailHelp" placeholder="Enter name"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1">LastName</label>
                            <input type="text" ref="lastName" className="form-control" id="exampleInputPassword1"
                                   placeholder="Last name"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1">Email</label>
                            <input type="email" ref="email" className="form-control" id="exampleInputPassword1"
                                   placeholder="Email"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1">Password</label>
                            <input type="password" ref="password" className="form-control" id="exampleInputPassword1"
                                   placeholder="Password"/>
                        </div>
                        <button className="btn btn-primary" onClick={this.changeProfile.bind(this)}>Change</button>
                    </div>
                </div>
            </>
        );
    }
}

export default MyProfile;
