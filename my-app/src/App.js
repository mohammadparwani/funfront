import React from 'react';
//import logo from './logo.svg';
import './App.css';


import Home from "./pages/Home";
import Champions from "./pages/Champions";
//import PageBuild from "./pages/Page build";
import Error from "./pages/ErrorPage";
import SingleChamp from './pages/SingleChamp';
import { Route, Switch } from 'react-router-dom';
import Navbar from './components/Navbar';
import Login from "./pages/Login"
import AddChamp from "./pages/AddChamp"
import ListOverView from "./pages/ListOverView";
import MyProfile from "./pages/MyProfile";
//import Login from "./Login"

function App() {
    return (
        <>
            <Navbar/>
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/Champions/" component={Champions} />
                    <Route exact path="/Champions/:slug" component={SingleChamp} />
                    <Route exact path="/AddChamp/" component={AddChamp} />
                    <Route exact path="/Login/" component={Login} />
                    <Route exact path="/ListOverView/" component={ListOverView} />
                    <Route exact path="/MyProfile/" component={MyProfile} />
                    <Route component={Error} />
                </Switch>
            </>
        )
}

export default App;
