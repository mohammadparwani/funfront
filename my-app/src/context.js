import React, { Component } from 'react';
import items from './data'

const ChampContext = React.createContext();
//RoomContext.Provider value={}

class ChampProvider extends Component {
    state = {
        champs: [],
        sortedChamps: [],
        featuredChamps: [],
        loading: true,
        type: 'all',
    };
    //getData{}

    componentDidMount() {
        let champs = this.formatData(items);
        let featuredChamps = champs.filter(champ => champ.featured === true);
        this.setState({
            champs,
            featuredChamps,
            sortedChamps: champs,
            loading: false
        });
    }

    formatData(items) {
        let tempItems = items.map(item => {
            let id = item.sys.id
            let images = item.fields.images.map(image => image.fields.file.url);

            let champ = { ...item.fields, images, id };
            return champ;
        });
        return tempItems;
    }
// Nu je een slug terug hebt gekregen zoek je specifiek naar een champ
    getChamp = (slug) => {
        let tempChamps = [...this.state.champs]
        const champ = tempChamps.find(champ => champ.slug === slug)
        return champ
    }

handleChange = event => {
  const target = event.target;
  const value = event.type === "checkbox" ? target.checked : target.value;
  const name = event.target.name;
  this.setState(
    {
    [name]: value
    },
    this.filterChamps
  );
};

//if not all filter else show all
filterChamps = () => {
  let {champs, type} = this.state

  let tempChamps = [...champs];
  if (type !== 'all') {
    tempChamps = tempChamps.filter(champ => champ.type === type)
  }
  this.setState({
    sortedChamps: tempChamps
  })
};
    render() {
        return (
            <ChampContext.Provider value={{ ...this.state, getChamp: this.getChamp, handleChange: this.handleChange }}>
                {this.props.children}
            </ChampContext.Provider>
        );
    }
}

const ChampConsumer = ChampContext.Consumer;

export function withChampConsumer(Component){
  return function ConsumerWrapper(props){
    return <ChampConsumer>
      {value => <Component {...props} context={value}/>}
    </ChampConsumer>
  }
}

export { ChampProvider, ChampConsumer, ChampContext };
